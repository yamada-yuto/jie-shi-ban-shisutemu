<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${loginUser.loginid}の設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>

<c:if test="${ empty loginUser }">
		<a href="login">ログイン</a>
	</c:if>


	<c:if test="${ not empty loginUser }">

	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="settings" method="post">
			<br /> <input name="id" value="${editUser.id}" id="id" type="hidden" />

			<label for="name">名前:</label> <input name="name"
				value="${editUser.name}" id="name" />（名前はあなたの公開プロフィールに表示されます）<br />

			<label for="loginid">ログインID:</label> <input name="loginid"
				value="${editUser.loginid}" /><br /> <label for="password">パスワード（変更）:</label>
			<input name="password" type="password" id="password" /> <br /> <label for="password2">パスワード(確認用):</label>
			<input name="password2" type="password" id="password2" /> <br />支店:<select
				name="branch">
				<c:forEach items="${branches}" var="branch">
					<option value="${branch.id}">${branch.branch}</option>
				</c:forEach>
			</select><br /> 部署・役職:<select name="division">
				<c:forEach items="${divisions}" var="division">
					<option value="${division.id}">${division.division}</option>
				</c:forEach>
			</select><br /> <input type="submit" value="登録" /> <br /> <a href="usermgmt">戻る</a>
		</form>

	</div>
		</c:if>
</body>
</html>