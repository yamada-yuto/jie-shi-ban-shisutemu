package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import beans.UserComment;
import dao.CommentDao;
import dao.MessageDao;
import dao.UserCommentDao;

public class CommentService {

	public void register(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao CommentDao = new CommentDao();
			CommentDao.insert(connection, comment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 140;

	public List<UserComment> getUserComment() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserCommentDao CommentDao = new UserCommentDao();
			List<UserComment> ret = CommentDao.getUserComment(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * @param commentId
	  */
	public void deleteComment(int comments_id) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao.delete(connection, comments_id);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 投稿の削除
	  * @param postId
	  */
	public void deleteMessage(int postId) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao.delete(connection, postId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 投稿のコメントを削除
	  */
	public void deleteMessageComment(int messages_id) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao.deleteMessageComment(connection, messages_id);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
