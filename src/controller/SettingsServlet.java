package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<User> users = new UserService().getUser();
		request.setAttribute("users", users);

		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		if (loginUser.getDivision() == 1 && loginUser.getBranch() == 1) {
			UserService service = new UserService();
			request.setAttribute("branches", service.getBranches());
			request.setAttribute("divisions", service.getDivisions());

			User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
			request.setAttribute("editUser", editUser);

			request.getRequestDispatcher("settings.jsp").forward(request, response);

		} else {

			List<String> messages = new ArrayList<String>();
			messages.add("アクセス権限がありません。");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("home");
		}

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			session.setAttribute("User", editUser);

			response.sendRedirect("./usermgmt");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		UserService service = new UserService();
		request.setAttribute("branches", service.getBranches());
		request.setAttribute("divisions", service.getDivisions());

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setLoginid(request.getParameter("loginid"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
		editUser.setDivision(Integer.parseInt(request.getParameter("division")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String loginid = request.getParameter("loginid");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");

		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if (name.length() >= 11) {
			messages.add("名前は10文字以内で入力してください");
		}

		if (StringUtils.isEmpty(loginid) == true) {
			messages.add("ログインidを入力してください");
		}
		if (loginid.length() < 6 || loginid.length() > 20) {
			messages.add("ログインidは6文字以上20文字以下で入力してください");
		}
		if (!(loginid.matches("[a-zA-Z0-9]+")) == true) {
			messages.add("ログインidは半角英数字で入力してください");
		}

		//		if (!password.matches("[-_@+*;:#$%&A-Za-z0-9]+")) {
		if (!(password.matches(".*[^\\\\u0020-\\\\u007E].*")) == false) {
			messages.add("パスワードは記号を含む全ての半角文字で入力してください");
		}
		if (!password.equals(password2)) {
			messages.add("パスワードは同じものを二度入力してください");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}