<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%--<%@page isELIgnored="false"%>--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ホーム</title>
</head>

<body>

	<c:if test="${ empty loginUser }">
		<a href="login">ログイン</a>
	</c:if>


	<c:if test="${ not empty loginUser }">


<div class = menu>
	・<a href="newMessage">新規投稿</a> 　・<a href="usermgmt">ユーザー管理</a>
	　・<a href="logout">ログアウト</a></div>
		<br>
		<br>

<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>


          <form action="home" method="get">
  <p>検索したいキーワードを入力してください。</p>
  <input type="search" name="search" placeholder="キーワードを入力">

<br>
<br>

日付 <br>
 <input type="date" name = oldDate >    <input type="date" name = newDate >
  <br>
   <input type="submit" name="submit" value="検索">
 </form>
 <br>
		<c:forEach items="${messages}" var="message">
			<div class="messages"> <div class="a">

				<div class="name">
					名前：<span class="name"><c:out value="${message.name}" /></span>
				</div>
				<div class="category">
					カテゴリー:
					<c:out value="${message.category}" />
				</div>
				<div class="subject">
					件名:
					<c:out value="${message.subject}" />
				</div>
				<div class="text">
					テキスト:
					<c:out value="${message.text}" />
				</div>
				<div class="date">
					<fmt:formatDate value="${message.created_date}"
						pattern="yyyy/MM/dd HH:mm:ss" />

				</div>

				<c:if test="${ message.user_id == loginUser.id}">
				<form action="deleteMessage" method="post"
					onClick="return postDialog()">
					<button class="btn" type="submit" name="deleteMessage"
						id="deleteMessage" value="${message.messages_id}">投稿削除</button>
				</form>
				</c:if>
</div>

				<c:forEach items="${comments}" var="comment">
					<c:if test="${ comment.messages_id == message.messages_id }">
						名前：<span class="name"><c:out value="${comment.name}" /></span>
						<div class="text">
							コメント：
							<c:out value="${comment.text}" />
						</div>
						<c:if test="${ comment.user_id == loginUser.id}">
						<form action="deleteComment" method="post"
							onClick="return commentDialog()">
							<button class="btn" type="submit" name="deleteComment"
								value="${comment.comments_id}">コメント削除</button>
						</form>
						</c:if>
					</c:if>
				</c:forEach>

				コメント
				<form action="newComment" method="post">
					<textarea name="text" cols="50" rows="1" class="tweet-box"></textarea><br>
					<input type="submit" value="コメント">（140文字まで） <input
						type="hidden" name="messages_id" value="${message.messages_id}">
				</form>
</div>

		</c:forEach>

	</c:if>

</body>
</html>
