package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> getUserComment(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.comments_id as comments_id, ");
			sql.append("comments.text as text, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("comments.messages_id as messages_id, ");
			sql.append("users.id as id, ");
			sql.append("users.name as name, ");
			sql.append("comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserCommentList(ResultSet rs)
			throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				int comments_id = rs.getInt("comments_id");
				String text = rs.getString("text");
				int user_id = rs.getInt("user_id");
				int messages_id = rs.getInt("messages_id");
				Timestamp createdDate = rs.getTimestamp("created_date");
				String name = rs.getString("users.name");

				UserComment comment = new UserComment();
				comment.setComments_id(comments_id);
				comment.setMessages_id(messages_id);
				comment.setUser_id(user_id);
				comment.setText(text);
				comment.setCreated_date(createdDate);
				comment.setName(name);
				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
