package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import beans.Division;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("loginid");
			sql.append(", name");
			sql.append(", branch");
			sql.append(", password");
			sql.append(", division");
			sql.append(") VALUES (");
			sql.append("?"); // loginid
			sql.append(", ?"); // name
			sql.append(", ?"); // branch
			sql.append(", ?"); // password
			sql.append(", ?"); // division
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginid());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch());
			ps.setString(4, user.getPassword());
			ps.setInt(5, user.getDivision());
			ps.executeUpdate();
			System.out.println(user.getDivision());
			System.out.println(user.getBranch());

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String loginid,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE loginid = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginid);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				String loginid = rs.getString("loginid");
				String name = rs.getString("name");
				int branch = rs.getInt("branch");
				String password = rs.getString("password");
				int division = rs.getInt("division");
				int stop = rs.getInt("stop");
				int id = rs.getInt("id");

				User user = new User();
				user.setLoginid(loginid);
				user.setName(name);
				user.setBranch(branch);
				user.setPassword(password);
				user.setDivision(division);
				user.setStop(stop);
				user.setId(id);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<Branch> getBranches(Connection conn) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branchlst";
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			return toBranchList(rs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs) throws SQLException {
		List<Branch> ret = new ArrayList<>();

		while (rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("branch");

			Branch branch = new Branch();
			branch.setId(id);
			branch.setBranch(name);

			ret.add(branch);
		}

		return ret;
	}

	public List<Division> getDivisions(Connection conn) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM divisionlst";
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			return toDivisionList(rs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Division> toDivisionList(ResultSet rs) throws SQLException {
		List<Division> ret = new ArrayList<>();

		while (rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("division");

			Division division = new Division();
			division.setId(id);
			division.setDivision(name);

			ret.add(division);
		}

		return ret;
	}

	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  loginid = ?");
			sql.append(", name = ?");
			sql.append(", branch = ?");
			sql.append(", division = ?");
			sql.append(", password = ?");
			sql.append(", stop = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginid());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch());
			ps.setInt(4, user.getDivision());
			ps.setString(5, user.getPassword());
			ps.setInt(6, user.getStop());
			ps.setInt(7, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void Stop(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" stop = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getStop());
			ps.setInt(2, user.getId());

			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
}
