package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class UsermgmtDao {

	public static List<User> getUser(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.loginid as loginid, ");
			sql.append("users.name as name, ");
			sql.append("users.branch as branch, ");
			sql.append("users.division as division, ");
			sql.append("users.stop as stop, ");
			sql.append("branchlst.branch as branch_name, ");
			sql.append("divisionlst.division as divisionName ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branchlst ");
			sql.append("ON users.branch = branchlst.id ");
			sql.append("INNER JOIN divisionlst ");
			sql.append("ON users.division = divisionlst.id ");

			sql.append("ORDER BY id ASC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private static List<User> toUserList(ResultSet rs)
			throws SQLException {

		List<User> ret = new ArrayList<User>();

		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String branch = rs.getString("branch_name");
				String division = rs.getString("divisionName");
				String loginid = rs.getString("loginid");
				int stop = rs.getInt("stop");

				User user = new User();
				user.setId(id);
				user.setName(name);
				user.setBranch_name(branch);
				user.setDivisionName(division);
				user.setLoginid(loginid);
				user.setStop(stop);
				ret.add(user);

			}

			return ret;
		} finally {
			close(rs);
		}
	}

}
