package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/home" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}
		List<UserMessage> messages = new MessageService().getMessage();
		List<UserComment> comments = new CommentService().getUserComment();

		String category = request.getParameter("search");
		String created_date1 = (request.getParameter("oldDate"));
		String created_date2 = (request.getParameter("newDate"));

		if (!(StringUtils.isEmpty(created_date1)) || !(StringUtils.isEmpty(created_date2))
				|| !(StringUtils.isEmpty(category))) {
			List<UserMessage> date = new MessageService().getDateOnly(category, created_date1, created_date2);
			request.setAttribute("messages", date);
			request.setAttribute("comments", comments);
			request.setAttribute("isShowMessageForm", isShowMessageForm);
			request.getRequestDispatcher("/home.jsp").forward(request, response);

		} else {
			request.setAttribute("messages", messages);
			request.setAttribute("comments", comments);
			request.setAttribute("isShowMessageForm", isShowMessageForm);
			request.getRequestDispatcher("/home.jsp").forward(request, response);
		}
	}
}
