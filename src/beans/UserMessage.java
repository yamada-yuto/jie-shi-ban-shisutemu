package beans;

import java.io.Serializable;
import java.util.Date;

public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private int messages_id;
    private String loginid;
    private String name;
    private String subject;
    private String category;
    private int user_id;
    private String text;
    private Date created_date;


	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getMessages_id() {
		return messages_id;
	}
	public void setMessages_id(int messages_id) {
		this.messages_id = messages_id;
	}


}