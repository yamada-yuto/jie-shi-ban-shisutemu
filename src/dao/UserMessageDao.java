package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.messages_id as messages_id, ");
			sql.append("messages.text as text, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("messages.category as category, ");
			sql.append("messages.subject as subject, ");
			sql.append("users.id as id, ");
			sql.append("users.name as name, ");
			sql.append("users.loginid as loginid, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String loginid = rs.getString("loginid");
				String name = rs.getString("name");
				String category = rs.getString("category");
				String subject = rs.getString("subject");
				int messages_id = rs.getInt("messages_id");
				int user_id = rs.getInt("user_id");
				String text = rs.getString("text");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessage message = new UserMessage();
				message.setLoginid(loginid);
				message.setName(name);
				message.setCategory(category);
				message.setSubject(subject);
				message.setMessages_id(messages_id);
				message.setUser_id(user_id);
				message.setText(text);
				message.setCreated_date(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<UserMessage> getDateOnly(Connection connection, String category, String created_date1,
			String created_date2) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.messages_id as messages_id, ");
			sql.append("messages.text as text, ");
			sql.append("messages.user_id as user_id, ");
			sql.append("messages.category as category, ");
			sql.append("messages.subject as subject, ");
			sql.append("users.id as id, ");
			sql.append("users.name as name, ");
			sql.append("users.loginid as loginid, ");
			sql.append("messages.created_date as created_date ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");

			if (!(StringUtils.isEmpty(created_date1)) && !(StringUtils.isEmpty(created_date2))
					&& !(StringUtils.isEmpty(category))) {
				sql.append("WHERE created_date BETWEEN ? and ? AND category LIKE ?");
				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, created_date1 + " 00:00:00");
				ps.setString(2, created_date2 + " 23:59:59");
				ps.setString(3, "%" + category + "%");

			} else if (!(StringUtils.isEmpty(created_date1)) && StringUtils.isEmpty(created_date2)
					&& !(StringUtils.isEmpty(category))) {
				sql.append("WHERE created_date BETWEEN ? and CURRENT_TIMESTAMP  AND category LIKE ?");
				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, created_date1 + " 00:00:00");
				ps.setString(2, "%" + category + "%");

			} else if (StringUtils.isEmpty(created_date1) && !(StringUtils.isEmpty(created_date2))
					&& !(StringUtils.isEmpty(category))) {
				sql.append("WHERE created_date BETWEEN '2019-04-01 00:00:00' and ?  AND category LIKE ?");
				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, created_date2 + "  23:59:59");
				ps.setString(2, "%" + category + "%");

			} else if (!(StringUtils.isEmpty(created_date1)) && !(StringUtils.isEmpty(created_date2))
					&& StringUtils.isEmpty(category)) {
				sql.append("WHERE created_date BETWEEN ? and ? ");
				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, created_date1 + " 00:00:00");
				ps.setString(2, created_date2 + " 23:59:59");

			} else if (!(StringUtils.isEmpty(created_date1)) && StringUtils.isEmpty(created_date2)
					&& StringUtils.isEmpty(category)) {
				sql.append("WHERE created_date BETWEEN ? and CURRENT_TIMESTAMP");
				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, created_date1 + " 00:00:00");

			} else if (StringUtils.isEmpty(created_date1) && !(StringUtils.isEmpty(created_date2))
					&& StringUtils.isEmpty(category)) {
				sql.append("WHERE created_date BETWEEN '2019-04-01 00:00:00' and ?");
				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, created_date2 + "  23:59:59");

			} else if (StringUtils.isEmpty(created_date1) && StringUtils.isEmpty(created_date2)
					&& !(StringUtils.isEmpty(category))) {
				sql.append(
						"WHERE created_date BETWEEN '2019-04-01 00:00:00' and CURRENT_TIMESTAMP  AND category LIKE ?");
				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, "%" + category + "%");
			}
			sql.append("ORDER BY created_date DESC ");

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
