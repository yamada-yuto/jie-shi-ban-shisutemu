package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("text");
			sql.append(", user_id");
			sql.append(", created_date");
			sql.append(", messages_id");
			sql.append(") VALUES (");
			sql.append(" ?"); // text
			sql.append(", ?"); // user_id
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", ?"); // messages_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getUser_id());
			ps.setInt(3, comment.getMessages_id());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public static void delete(Connection connection, int comments_id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments WHERE comments_id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, comments_id);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * 指定された投稿IDに対応するコメントをすべて削除
	  * @param connection
	  * @param postId
	  */
	public static void deleteMessageComment(Connection connection, int messages_id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments WHERE messages_id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, messages_id);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}