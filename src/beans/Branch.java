package beans;

import java.io.Serializable;

public class Branch implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String branch;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}

}