package beans;

import java.io.Serializable;

	public class Division implements Serializable {
		private static final long serialVersionUID = 1L;

		private int id;
		private String division;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getDivision() {
			return division;
		}
		public void setDivision(String division) {
			this.division = division;
		}

}
