package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("text");
			sql.append(", category");
			sql.append(", subject");
			sql.append(", created_date");
			sql.append(", user_id");
			sql.append(") VALUES (");
			sql.append(" ?"); //text
			sql.append(", ?"); // category
			sql.append(", ?"); // subject
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", ?"); // id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, message.getText());
			ps.setString(2, message.getCategory());
			ps.setString(3, message.getSubject());
			ps.setInt(4, message.getUser_id());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * 投稿の削除
	  * @param connection
	  * @param postId
	  */
	public static void delete(Connection connection, int messages_id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM messages WHERE messages_id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, messages_id);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}