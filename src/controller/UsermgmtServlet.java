package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/usermgmt" })
public class UsermgmtServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<User> users = new UserService().getUser();
		request.setAttribute("users", users);

		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");

		if (loginUser.getDivision() == 1 && loginUser.getBranch() == 1) {

			request.getRequestDispatcher("usermgmt.jsp").forward(request, response);

		} else {

			List<String> messages = new ArrayList<String>();
			messages.add("アクセス権限がありません。");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("home");
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		User users = new User();

		users.setStop(Integer.parseInt(request.getParameter("stop")));
		users.setId(Integer.parseInt(request.getParameter("id")));

		new UserService().stop(users);
		request.setAttribute("users", users);
		response.sendRedirect("usermgmt");

	}
}
