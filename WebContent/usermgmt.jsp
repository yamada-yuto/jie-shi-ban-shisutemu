<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%--<%@page isELIgnored="false"%>--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="cssファイルのパス" rel="stylesheet" type="text/css">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/test.js"></script>
<title>ユーザー管理</title>
</head>
<script>
	function activeDialog() {
		var txt;
		var rt = confirm("本当に停止させますか？");
		if (rt == true) {
			txt = "「ok」を選択しました。";
			return true;
		} else {
			txt = "「キャンセル」を選択しました。";
			return false;
		}
		document.getElementById("conf1").innerHTML = txt;
	}
	function stopDialog() {
		var txt;
		var rt = confirm("本当に復活させますか？");
		if (rt == true) {
			txt = "「ok」を選択しました。";
			return true;
		} else {
			txt = "「キャンセル」を選択しました。";
			return false;
		}
		document.getElementById("conf2").innerHTML = txt;
	}
</script>
<body>
	<c:if test="${ empty loginUser }">
		<a href="login">ログイン</a>
	</c:if>


	<c:if test="${ not empty loginUser }">


	・<a href="home">ホーム</a>・
	<a href="signup">ユーザー新規登録</a>
		<br>
		<br>
		<table border="1">
			<tr>

				<th>ログインID</th>
				<th>名前</th>
				<th>支店</th>
				<th>部署・役職</th>
				<th>設定</th>
			</tr>


			<c:forEach items="${users}" var="user">
				<tr>
					<td><c:out value="${user.loginid}" /></td>
					<td><c:out value="${user.name}" /></td>
					<td><c:out value="${user.branch_name}" /></td>
					<td><c:out value="${user.divisionName}" /></td>
					<td><a href="settings?id=${user.id}">編集</a> ・
						<form action="usermgmt" method="post"
							class="col-lg-2 col-lg-offset-1">

							<c:if test="${ user.stop == 0 }">
								<button class="btn" type="submit" name="stop" value="1"
									onClick="return activeDialog()">停止</button>
								<input type="hidden" name="id" value="${user.id}">
							</c:if>

							<c:if test="${ user.stop == 1 }">
								<button class="btn" type="submit" name="stop" value="0"
									onClick="return stopDialog()">復活</button>
								<input type="hidden" name="id" value="${user.id}">
							</c:if>

						</form></td>

				</tr>

			</c:forEach>

		</table>
		<a href="home">戻る</a>
	</c:if>
</body>
</html>