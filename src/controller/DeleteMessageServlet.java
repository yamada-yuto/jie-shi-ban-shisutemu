package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Message;
import service.CommentService;
import service.MessageService;

/**
 * 投稿削除
 * @author urushibata.takumi
 */
@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		Message message = new Message();
		message.setText(request.getParameter("messages_id"));

		int messages_id = Integer.parseInt(request.getParameter("deleteMessage"));

		new MessageService().deleteMessage(messages_id);
		new CommentService().deleteMessageComment(messages_id);

		response.sendRedirect("./home");
	}

}
